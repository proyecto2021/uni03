/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.uni03.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author xtian
 */
@Entity
@Table(name = "uni03")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Uni03.findAll", query = "SELECT u FROM Uni03 u"),
    @NamedQuery(name = "Uni03.findByRut", query = "SELECT u FROM Uni03 u WHERE u.rut = :rut"),
    @NamedQuery(name = "Uni03.findByNombre", query = "SELECT u FROM Uni03 u WHERE u.nombre = :nombre"),
    @NamedQuery(name = "Uni03.findByApellido", query = "SELECT u FROM Uni03 u WHERE u.apellido = :apellido"),
    @NamedQuery(name = "Uni03.findByGenero", query = "SELECT u FROM Uni03 u WHERE u.genero = :genero"),
    @NamedQuery(name = "Uni03.findByTelefono", query = "SELECT u FROM Uni03 u WHERE u.telefono = :telefono")})
public class Uni03 implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "rut")
    private String rut;
    @Size(max = 2147483647)
    @Column(name = "nombre")
    private String nombre;
    @Size(max = 2147483647)
    @Column(name = "apellido")
    private String apellido;
    @Size(max = 2147483647)
    @Column(name = "genero")
    private String genero;
    @Size(max = 2147483647)
    @Column(name = "telefono")
    private String telefono;

    public Uni03() {
    }

    public Uni03(String rut) {
        this.rut = rut;
    }

    public String getRut() {
        return rut;
    }

    public void setRut(String rut) {
        this.rut = rut;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apelllido) {
        this.apellido = apelllido;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (rut != null ? rut.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Uni03)) {
            return false;
        }
        Uni03 other = (Uni03) object;
        if ((this.rut == null && other.rut != null) || (this.rut != null && !this.rut.equals(other.rut))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mycompany.uni03.entity.Uni03[ rut=" + rut + " ]";
    }
    
}
