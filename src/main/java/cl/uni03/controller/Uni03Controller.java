/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.uni03.controller;

import com.mycompany.uni03.entity.Uni03;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author xtian
 */
@WebServlet(name = "Uni03Controller", urlPatterns = {"/Uni03Controller"})
public class Uni03Controller extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet Uni03Controller</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet Uni03Controller at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
      
        String accion = request.getParameter("accion");
        
        if (accion.equals("Ingreso")){
            request.getRequestDispatcher("crear.jsp").forward(request, response);
            
        }
        
        if (accion.equals("ingreso")){
            Uni03 uni03 = new Uni03();
            uni03.setRut(request.getParameter("rut"));
            uni03.setNombre(request.getParameter("nombre"));
            uni03.setApellido(request.getParameter("apellido"));
            uni03.setGenero(request.getParameter("genero"));
            uni03.setTelefono(request.getParameter("telefono"));
            Client client = ClientBuilder.newClient();
            WebTarget myResource1 = client.target("http://xtianpc:8080/uni03-1.0-SNAPSHOT/api/uni03");
            Uni03 uni031 = myResource1.request(MediaType.APPLICATION_JSON).post(Entity.json(uni03),Uni03.class);
        
            request.getRequestDispatcher("index.jsp").forward(request, response);    
        }
        
        //lista
         if (accion.equals("listar")){
           // Uni03 uni03 = new Uni03();
            Client client = ClientBuilder.newClient();
            WebTarget myResource = client.target("http://xTianPc:8080/uni03-1.0-SNAPSHOT/api/uni03");
            List<Uni03> lista = (List<Uni03>) myResource.request(MediaType.APPLICATION_JSON).get(new GenericType<List<Uni03>>(){
            });
            
            request.setAttribute("lista", lista);
            request.getRequestDispatcher("lista.jsp").forward(request, response); 
            
        }
         
         //eliminar
         if (accion.equals("eliminar")){
            String idEliminar = request.getParameter("seleccion");
            Client client = ClientBuilder.newClient();
            WebTarget myResource = client.target("http://xTianPc:8080/uni03-1.0-SNAPSHOT/api/uni03/"+idEliminar);
            myResource.request(MediaType.APPLICATION_JSON).delete();
          request.getRequestDispatcher("index.jsp").forward(request, response); 
            
            
        }
         
         if (accion.equals("ver")){
            String idConsultar = request.getParameter("seleccion");
            Client client = ClientBuilder.newClient();
            WebTarget myResource = client.target("http://xTianPc:8080/uni03-1.0-SNAPSHOT/api/uni03/"+idConsultar);
            Uni03 uni03 = myResource.request(MediaType.APPLICATION_JSON).get(Uni03.class);
            //  request.getRequestDispatcher("index.jsp").forward(request, response); 
            request.setAttribute("uni03", uni03);
            request.getRequestDispatcher("editar.jsp").forward(request, response); 
            
        }
         
        if(accion.equals("index")){
            
          request.getRequestDispatcher("index.jsp").forward(request, response);
      }
        
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
