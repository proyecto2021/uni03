<%-- 
    Document   : index
    Created on : 1/05/2021, 10:53:27 PM
    Author     : xtian
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    </head>
    <body>
        <form name="form" action="Uni03Controller" method="POST">
        <h1> Christian Herrera seccion 50 </h1>
        
        <button type="submit" name="accion" value="listar" class="btn btn-info">consultar y eliminar</button>
        <button type="submit" name="accion" value="Ingreso" class="btn btn-primary">Crear Persona</button>
        <button type="submit" name="accion" value="ver" class="btn btn-success">Modificar Persona</button>
      <%--   <button type="submit" name="accion" value="eliminar" class="btn btn-danger">Eliminar Persona</button>--%>
         <hr> 
             </hr>
             <h2>Links HEROKU</h2>
             <hr> 
             </hr>
         <div class="col">
            <p><a class="p-3 mb-2 bg-info text-white">Url GET : https://uni03christianherrera.herokuapp.com/api/uni03</a></p>
        </div>    
             <hr> 
             </hr>
        <div class="col">
            <p><a class="p-3 mb-2 bg-primary text-white">Url POST : https://uni03christianherrera.herokuapp.com/api/uni03</a></p>
        </div>      
             <hr> 
             </hr>
        <div class="col">
            <p><a class="p-3 mb-2 bg-success text-white">Url PUT : https://uni03christianherrera.herokuapp.com/api/uni03</a></p></div>
        </div>      
             <hr> 
             </hr>
        <div class="col">
            <p><a class="p-3 mb-2 bg-danger text-white">Url DELETE : https://uni03christianherrera.herokuapp.com/api/uni03/{iddelete}</a></p><!-- comment -->
        </div> 
             <hr> 
             </hr>
             <h2>Links LOCALES</h2>
             <hr> 
             </hr>
         <div class="col">
            <p><a class="p-3 mb-2 bg-info text-white">Url GET : http://xTianPc:8080/uni03-1.0-SNAPSHOT/api/uni03</a></p>
        </div>    
             <hr> 
             </hr>
        <div class="col">
            <p><a class="p-3 mb-2 bg-primary text-white">Url POST : http://xTianPc:8080/uni03-1.0-SNAPSHOT/api/uni03</a></p>
        </div>      
             <hr> 
             </hr>
        <div class="col">
            <p><a class="p-3 mb-2 bg-success text-white">Url PUT : http://xTianPc:8080/uni02-1.0-SNAPSHOT/api/uni03</a></p></div>
        </div>      
             <hr> 
             </hr>
        <div class="col">
            <p><a class="p-3 mb-2 bg-danger text-white">Url DELETE : http://xTianPc:8080/uni03-1.0-SNAPSHOT/api/uni03/{iddelete}</a></p><!-- comment -->
        </div> 
             <hr> 
             </hr>
       </form> 
    </body>
</html>
