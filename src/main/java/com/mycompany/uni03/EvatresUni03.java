/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.uni03;

import com.mycompany.uni03.dao.Uni03JpaController;
import com.mycompany.uni03.dao.exceptions.NonexistentEntityException;
import com.mycompany.uni03.entity.Uni03;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author xtian
 */

@Path("uni03")
public class EvatresUni03 {
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response listarSolicitudes(){
        
    /*Uni03 uni1 = new Uni03();
    uni1.setRut("19");
    uni1.setNombre("hola uni3");
    
    List<Uni03> lista= new ArrayList<Uni03>();
    
     lista.add(uni1);
     */
    
    Uni03JpaController dao = new Uni03JpaController();
    
    List<Uni03> lista= dao.findUni03Entities();
     return Response.ok(200).entity(lista).build();
    }
    
    @GET
    @Path("/(idbuscar)")
    @Produces(MediaType.APPLICATION_JSON)
    public Response consultarPorRut(@PathParam("idbuscar")String idbusca){
    
      
    Uni03JpaController dao = new Uni03JpaController();
    
    Uni03 uni03= dao.findUni03(idbusca);
     return Response.ok(200).entity(uni03).build();
    }
    
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response add(Uni03 uni03){
    Uni03JpaController dao = new Uni03JpaController();
        try {
            dao.create(uni03);
        } catch (Exception ex) {
            Logger.getLogger(EvatresUni03.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return Response.ok(200).entity(uni03).build();
    }
    
    
    @DELETE
    @Path("/{iddelete}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response delete(@PathParam("iddelete")String iddelete){
        try {
            Uni03JpaController dao = new Uni03JpaController();
            dao.destroy(iddelete);
        } catch (NonexistentEntityException ex) {
            Logger.getLogger(EvatresUni03.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return Response.ok("cliente eliminado").build();
  }
    
    
    @PUT
    public Response update(Uni03 uni03){
        
        try {
            Uni03JpaController dao = new Uni03JpaController();
            dao.edit(uni03);
        } catch (Exception ex) {
            Logger.getLogger(EvatresUni03.class.getName()).log(Level.SEVERE, null, ex);
        }
       return Response.ok(200).entity(uni03).build(); 
    }
    
}