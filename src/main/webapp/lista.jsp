<%-- 
    Document   : lista
    Created on : 2/05/2021, 01:54:22 PM
    Author     : xtian
--%>

<%@page import="java.util.Iterator"%>
<%@page import="com.mycompany.uni03.entity.Uni03"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<%
    List<Uni03> lista = (List<Uni03> )request.getAttribute("lista");
    Iterator<Uni03> itUni03 = lista.iterator();    
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style>
table {
    border-collapse: collapse;
    width: 50%;
}
th, td {
    text-align: left;
    padding: 4px;
}
tr:nth-child(even){background-color: #f2f2f2}
th {
    background-color: #4CAF50;
    color: white;
}
.main-wrapper{
    width:90%;
    
    background:#E0E4E5;
    border:1px solid #292929;
    padding:25px;
    margin:auto;
}
hr {
    margin-top: 5px;
    margin-bottom: 5px;
    border: 0;
    border-top: 1px solid #eee;
}
h2{
    font-size:24px;
    }
</style>
    </head>
    <body>
      <%--   <form name="form" action="BaseController" method="POST">
      <form name="form" action="uni03" method="GET">--%>
          <form name="form" action="Uni03Controller" method="POST">
            <div class="cover-container d-flex h100 p-3 mx-auto flex-column">
                 <h1> Lista o tabla </h1>
                <table border="1">
                    <thead>
                <%--    <th>Rut</th>
                    <th>Nombre</th>
                    <th>Apellido</th><!-- comment -->
                    <th>Genero</th>
                    <th>Telefono</th>
                    <th width="10%">Rut</th>--%>
        <th width="10%">Rut</th>       
        <th width="10%">Nombres</th>
        <th width="10%">Apellidos</th>
        <th width="10%">Genero</th>
        <th width="10%">Telefono</th>
        <th width="10%">Op</th>
                    </thead>

                    <tbody>
                        <%while (itUni03.hasNext()) {
                           Uni03 per = itUni03.next();%>
                        <tr>
                            <td><%= per.getRut()%></td>
                            <td><%= per.getNombre()%></td><!-- comment -->
                            <td><%= per.getApellido()%></td>
                            <td><%= per.getGenero()%></td>
                            <td><%= per.getTelefono()%></td>
                            
                            <td><input type="radio" name="seleccion" value="<%= per.getRut()%>"></td>
                        </tr>
                        <%}%>
                    </tbody> 
                </table>
          <button type="submit" name="accion" value="ver" class="btn btn-success">ver solicitud </button>         
        <button type="submit" name="accion" value="eliminar" class="btn btn-success">eliminar </button>
          <button type="submit" name="accion" value="index" class="btn btn-success">volver a inicio </button>
        </form>   
            
       <HR SIZE=5 WIDTH=50% ALIGN=LEFT>
       
       
    </body>
</html>
