/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.uni03.dao;

import com.mycompany.uni03.dao.exceptions.NonexistentEntityException;
import com.mycompany.uni03.dao.exceptions.PreexistingEntityException;
import com.mycompany.uni03.entity.Uni03;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author xtian
 */
public class Uni03JpaController implements Serializable {

    public Uni03JpaController() {
        
    }
    private EntityManagerFactory emf = Persistence.createEntityManagerFactory("my_persistence_unit");

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Uni03 uni03) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(uni03);
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findUni03(uni03.getRut()) != null) {
                throw new PreexistingEntityException("Uni03 " + uni03 + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Uni03 uni03) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            uni03 = em.merge(uni03);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = uni03.getRut();
                if (findUni03(id) == null) {
                    throw new NonexistentEntityException("The uni03 with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Uni03 uni03;
            try {
                uni03 = em.getReference(Uni03.class, id);
                uni03.getRut();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The uni03 with id " + id + " no longer exists.", enfe);
            }
            em.remove(uni03);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Uni03> findUni03Entities() {
        return findUni03Entities(true, -1, -1);
    }

    public List<Uni03> findUni03Entities(int maxResults, int firstResult) {
        return findUni03Entities(false, maxResults, firstResult);
    }

    private List<Uni03> findUni03Entities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Uni03.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Uni03 findUni03(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Uni03.class, id);
        } finally {
            em.close();
        }
    }

    public int getUni03Count() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Uni03> rt = cq.from(Uni03.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
